// dracula.c
// Implementation of your "Fury of Dracula" Dracula AI

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "Game.h"
#include "DracView.h"

typedef int code;

#define ROUTE_SIZE 11
#define STARTING_AREAS 5

#define TS_CAG 0//goes through ionian
#define TS_ATHENS 1//ionian sea>athens>valona
#define TS_VALONALOOP_V1 2
#define TS_VALONALOOP_V2 3
#define TS_LONDONLOOP 4 //may have to include EC to stall
#define TS_MADRIDLOOP 5
#define TS_HPCYCLE 6// heals 30(40), trail is CD-KL-GA-Dn(10)-HIDE(10)-TP(10)
#define TS_WATER 7
#define FT_GALWAY_R 10//ESCAPE LONDONLOOP R=from right side
#define FT_GALWAY_L 11
#define FT_EDINBURGH 12//ESCAPE LONDONLOOP man>edin>NS
#define FT_PLYMOUTH 13//ESCAPE LONDONLOOP lon>ply>EC
#define FT_AMSTERDAM 14//ESCAPE LONDON LOOP (sort of) NS>amst>brussels||cologne
#define FT_FLORENCE 15//ESCAPE CAG  TS> rom||ven||gen>flor can be accessed from adriatic too
#define FT_BARCELONA 16//ESCAPE CAG  med||barc||sarg||toul>db

#define PLAN_MOD 0
#define DB_MOD 1
#define HIDE_MOD 2 
#define TP_MOD 3 

char *playCode(int action);
LocationID followRoute (LocationID currLoc, LocationID route[], char *plan);
void getRoute (LocationID route[ROUTE_SIZE], code routeID);
LocationID nextInRoute (LocationID loc, LocationID route[]);
void fillArray (int path[ROUTE_SIZE], LocationID a, LocationID b, LocationID c, 
				LocationID d, LocationID e, LocationID f, LocationID g, 
				LocationID h, LocationID i, LocationID j, LocationID k);
void constructMessage(char str[], int plan, int doubleBack, int hide, int teleport);

void decideDraculaMove(DracView gameState)
{
	char planID = '3'; //THIS WILL BREAK - fix soon
	//printf("%d\n", (planID-48));
	char doubleBack ='0';
	char hide ='0';
	char teleport = '0';
	LocationID action;
	LocationID desiredRoute[ROUTE_SIZE];
	char *plan = myPlan(gameState);

	if (whereIs(gameState, PLAYER_DRACULA) == NOWHERE){
printf("whereIs(gameState, PLAYER_DRACULA = %d)\n",whereIs(gameState, PLAYER_DRACULA));
		registerBestPlay("VA", "2000");
	} else {
		getRoute (desiredRoute, (planID-48));//THIS WILL BREAK - fix soon
/*
printf("[%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d]\n", 
	desiredRoute[0], desiredRoute[1], desiredRoute[2], desiredRoute[3], desiredRoute[4]
	, desiredRoute[5], desiredRoute[6], desiredRoute[7], desiredRoute[8]
	, desiredRoute[9], desiredRoute[10]);
*/
		action = followRoute(whereIs(gameState, PLAYER_DRACULA), desiredRoute, plan);

//printf("action is %d\n", action);
		char *bestPlay = playCode(action);
//printf("bestPlay is %s\n", bestPlay);

		char str[7];

		
		switch (action) {
			case DOUBLE_BACK_1: doubleBack = '1'; break; //changed to 'broken' values 1=2,2=3etc
			case DOUBLE_BACK_2: doubleBack = '2'; break;
			case HIDE: hide = '1'; break;
			case TELEPORT: teleport = '1'; break;
		}

		constructMessage(str, planID ,doubleBack ,hide ,teleport);
printf("whereIs(gameState, PLAYER_DRACULA = %d)\n",whereIs(gameState, PLAYER_DRACULA));
		registerBestPlay(bestPlay,str);		
	}
}

void constructMessage(char str[],int planID, int doubleBack, int hide, int teleport){
	str[0] = planID;
	str[1] = doubleBack;
	str[2] = hide;
	str[3] = teleport;
	str[4] = '\0';
}

char *playCode(int action){
	if (action == 100){
		return "C?";
	} else if (action == 101) {
		return "S?";
	} else if (action == 102) {
		return "HI";
	} else if (action == 103) {
		return "D1";
	} else if (action == 104) {
		return "D2";
	} else if (action == 105) {
		return "D3";
	} else if (action == 106) {
		return "D4";
	} else if (action == 107) {
		return "D5";
	} else if (action == 108) {
		return "TP";
	} else if (action == -1) {
		return "TP";//asdfasdfasdf SHOULD BE NO WHERE
	} else {
		return idToAbbrev(action);//asdfasdfasdf
	}
}

//"special" moves
LocationID followRoute (LocationID currLoc, LocationID route[], char *plan){
	int i = 0;

	if (plan[DB_MOD] == '1') { //fixed to meet 'broken' values
		currLoc = DOUBLE_BACK_1;
	} else if (plan[DB_MOD] == '2') {
		currLoc = DOUBLE_BACK_2;
	} else if (plan[HIDE_MOD] == '1') {
		currLoc = HIDE;
	} else if (plan[TP_MOD] == '1') {
		currLoc = CASTLE_DRACULA;
	}

	while (i < ROUTE_SIZE) {
		if (currLoc == route[i]) {
		return route[i+1];

		}
		i++;
	}
	return NOWHERE;
}

//Gets the next location from the route Drac is taking
LocationID nextInRoute (LocationID loc, LocationID route[]) {
	int counter = 0;
	while(loc != route[counter]) {
		counter++;
	}
	return route[counter+1];
}

//Possible routes from Drac to take
void getRoute (LocationID route[ROUTE_SIZE], code routeID)
{

	 switch(routeID){
	    case TS_CAG: fillArray(route, CASTLE_DRACULA, GALATZ, CONSTANTA, BLACK_SEA, IONIAN_SEA, 
	    					TYRRHENIAN_SEA, CAGLIARI, MEDITERRANEAN_SEA, DOUBLE_BACK_2, HIDE, TELEPORT);
	    		break;

	    case TS_ATHENS: fillArray(route, CASTLE_DRACULA, GALATZ, CONSTANTA, BLACK_SEA, IONIAN_SEA, 
	    					ATHENS, VALONA, DOUBLE_BACK_2, HIDE, TELEPORT, NOWHERE);
	    		break;

	    case TS_VALONALOOP_V1: fillArray(route, VALONA, SARAJEVO, DOUBLE_BACK_2, HIDE, SALONICA, 
	    					SOFIA, VALONA, NOWHERE, NOWHERE, NOWHERE, NOWHERE);
	    		break;

	    case TS_VALONALOOP_V2: fillArray(route, VALONA, HIDE, ATHENS, DOUBLE_BACK_1, SALONICA, 
	    					SOFIA, SARAJEVO, VALONA, NOWHERE, NOWHERE, NOWHERE);
	    		break;

	    case TS_LONDONLOOP: fillArray(route, IRISH_SEA, LIVERPOOL, HIDE, MANCHESTER, LONDON, 
	    					SWANSEA, IRISH_SEA, NOWHERE, NOWHERE, NOWHERE, NOWHERE);
	    		break;

	    case TS_MADRIDLOOP: fillArray(route, ALICANTE, HIDE, GRANADA, CADIZ, LISBON, 
	    					MADRID, ALICANTE, NOWHERE, NOWHERE, NOWHERE, NOWHERE);
	    		break;
	    //if hide was last move used to reach here, cd can heal up to 40
	    case TS_HPCYCLE: fillArray(route, CASTLE_DRACULA, GALATZ, KLAUSENBURG, DOUBLE_BACK_3, 
	    					HIDE, NOWHERE, NOWHERE, NOWHERE, NOWHERE, NOWHERE, NOWHERE);
	    		break;

		case TS_WATER: fillArray(route, ATLANTIC_OCEAN, NORTH_SEA, ENGLISH_CHANNEL, LE_HAVRE, NANTES, 
	    					BAY_OF_BISCAY, ATLANTIC_OCEAN, NOWHERE, NOWHERE, NOWHERE, NOWHERE);
	    		break;

	    //force tp below

	    case FT_GALWAY_L: fillArray(route, ATLANTIC_OCEAN, GALWAY, DUBLIN, DOUBLE_BACK_2, HIDE,
	    					NOWHERE, NOWHERE, NOWHERE, NOWHERE, NOWHERE, NOWHERE);
	    		break;

	    case FT_GALWAY_R: fillArray(route, IRISH_SEA, DUBLIN, GALWAY, DOUBLE_BACK_2, HIDE, 
	    					NOWHERE, NOWHERE, NOWHERE, NOWHERE, NOWHERE, NOWHERE);
	    		break;

	    case FT_EDINBURGH: fillArray(route, MANCHESTER, EDINBURGH, NORTH_SEA, DOUBLE_BACK_2, HIDE,
	    					NOWHERE, NOWHERE, NOWHERE, NOWHERE, NOWHERE, NOWHERE);
	    		break;

	    case FT_PLYMOUTH: fillArray(route, LONDON, PLYMOUTH, ENGLISH_CHANNEL, DOUBLE_BACK_2, HIDE,
	    					NOWHERE, NOWHERE, NOWHERE, NOWHERE, NOWHERE, NOWHERE);
	    		break;

	    case FT_AMSTERDAM: fillArray(route, NORTH_SEA, AMSTERDAM, BRUSSELS, COLOGNE, DOUBLE_BACK_3,
	    					HIDE, NOWHERE, NOWHERE, NOWHERE, NOWHERE, NOWHERE);
	    		break;

	    case FT_FLORENCE: fillArray(route, TYRRHENIAN_SEA, GENOA, VENICE, FLORENCE, ROME, 
	    					DOUBLE_BACK_2, HIDE, NOWHERE, NOWHERE, NOWHERE, NOWHERE);
	    		break;

	    case FT_BARCELONA: fillArray(route, MEDITERRANEAN_SEA, BARCELONA, SARAGOSSA, TOULOUSE,
	    					DOUBLE_BACK_3, HIDE, NOWHERE, NOWHERE, NOWHERE, NOWHERE, NOWHERE);
	    		break;

	    default: fillArray(route, NOWHERE, NOWHERE, NOWHERE, NOWHERE, NOWHERE, NOWHERE, NOWHERE,
	    					NOWHERE, NOWHERE, NOWHERE, NOWHERE);
	}
}



void fillArray (int path[ROUTE_SIZE], LocationID a, LocationID b, LocationID c, LocationID d,
				LocationID e, LocationID f, LocationID g, LocationID h, LocationID i, 
				LocationID j, LocationID k) 
{
	/////////####////////printf("[%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d]\n", a,b,c,d,e,f,g,h,i,j,k);
	int counter;
	for (counter = 0; counter < ROUTE_SIZE; counter++) {
		switch(counter) {
			case 0: path[counter] = a; break;
			case 1: path[counter] = b; break;
			case 2: path[counter] = c; break;
			case 3: path[counter] = d; break;
			case 4: path[counter] = e; break;
			case 5: path[counter] = f; break;
			case 6: path[counter] = g; break;
			case 7: path[counter] = h; break;
			case 8: path[counter] = i; break;
			case 9: path[counter] = j; break;
			case 10: path[counter] = k; break;
		}
	}
	
}