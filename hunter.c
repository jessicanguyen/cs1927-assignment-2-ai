// hunter.c
// Implementation of your "Fury of Dracula" hunter AI
// Group: ByteMe  of COMP1927, S2_2014

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <assert.h>
#include "Places.h"
#include "Game.h"
#include "HunterView.h"
#include "Map.h"

#define TRUE 1
#define FALSE 0
#define A_INIT -256826
// not trusting rail moves any more
#define NO_RAIL_MOVES 0

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 
// @@@  HEADER DECLARATIONS
typedef struct QueueRep *Queue;
typedef struct QueueNode {
	Edge value;
	struct QueueNode *next;
} QueueNode;

typedef struct QueueRep {
	QueueNode *head;  // ptr to first node
	QueueNode *tail;  // ptr to last node
} QueueRep;

static void printDebug(HunterView gs);
static void rmFromArray(int *array, int v);
static int existInArray(int *array, int v);
// static int pop(int *array);
static void append(int *array, int v);
// static int arrayLength(int *array);
// static int lastNode(int *array);
static void shortestPath(LocationID start, LocationID end, 
    LocationID path[200], LocationID bans[100], LocationID *numLocs, 
    int road, int rail, int sea);
static int transportType(Map g, int source, int target, int ttype);
static int existInList(Map g, int source, int target);
static Queue newQueue();
static void dropQueue(Queue Q);
static Edge QueueLeave(Queue Q);
static void QueueJoin(Queue Q, Edge it);
static int QueueIsEmpty(Queue Q);

static int prng_seed = FALSE;   // doubt itll be useful..

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 
// @@@ Grand list of TODOs (in no particular order)
// - reading messages?
// - vampire kill detection and announcement
// - output message toggle


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 
// @@@ MESSAGE CODES
//     TODO toggle for msgcode or readable message 
//  message toggle: 0-passcode  1-fancy
//     #define MSG_TOGGLE 0
//
// #1000  "hello"
// #0001  "enroute to drac!"
// #0002  "close to fighting drac"
// #0003  "random move"
// #0004  "oops dead"
// #0005  "should be fighting drac"
// #0006  "resting"
// #0007  "research"
// #0008  "pls dont kill me drac"
// 
// error codes
// ##0100  "error. relocating"
//


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 

void decideHunterMove(HunterView gs)
{
    assert(whoAmI(gs) != PLAYER_DRACULA);
    printf("\n--- ByteMe Hunter AI ver1130-17 ---\n");   // ver MMDD-HH (24hr time, MM = MM+1)
    
    
    // TESTING ENVIRONMENT
    /*
    { printf("\n@@@@@ testing environment @@@@@\n");
        printDebug(gs);

        return;
    } */
    
    // PRNG seeding/ setup
    if (!prng_seed) {
        srand(time(NULL));
        prng_seed = TRUE;
    }

    // PRNG-based SPAWN location picker
    //   rerolls capped at (SPAWN_ROLL_CAP)
    //   minimal distance from other hunter > (MIN_DISTANCE)
    #define SPAWN_ROLL_CAP 50
    #define MIN_DISTANCE 2
    if (giveMeTheRound(gs) == 0) {
        PlayerID me = whoAmI(gs); // int i; 
        if (me > 0) {
            LocationID prevLocs[10];
            int i; for (i = 0; i < 10; i++) prevLocs[i] = A_INIT;
            LocationID toGo = UNKNOWN_LOCATION;
            for (i = 0; i < me; i++) {
                prevLocs [i] = whereIs(gs, i);
            }
            for (i = 0; i < 50; i++) {
                toGo = rand()%NUM_MAP_LOCATIONS;
                if (existInArray(prevLocs, toGo)) continue;
                int j, validSpawn = FALSE;
                for (j = 0; j < me; j++) {
                    int m, numLocs = 0;
                    LocationID path[200]; for (m = 0; m < 200; m++) path[m] = A_INIT;
                    LocationID bans[10] = {A_INIT, A_INIT};
                    shortestPath(toGo, prevLocs[j], path, bans, 
                        &numLocs, 1, NO_RAIL_MOVES, 1);
                    if (numLocs > MIN_DISTANCE) {
                        validSpawn = TRUE; break;
                    }
                }
                if (validSpawn) break;
            }
            if (i == 50) toGo = rand()%NUM_MAP_LOCATIONS; 

            registerBestPlay(idToAbbrev(toGo), "hello");
        } else {
            registerBestPlay(idToAbbrev(rand()%NUM_MAP_LOCATIONS), "hello");
        }
        
        printDebug(gs);
        return;
    }


    // @@@@@   DEBUGGING PRINTFs   @@@@@
    //   feel free to comment out to disable
    printDebug(gs);
    
    
    // RESTING mechanism
    //   moves to ST_JOSEPH_AND_ST_MARYS if dead
    //   rests (no move) if health < 5
    //   rollcap is at 30
    {
        PlayerID me = whoAmI(gs);
        int myHealth = howHealthyIs(gs, me);
        if (myHealth == 0) {
            registerBestPlay("JM", "0004");
            return;
        }
        
        LocationID dracTrail[6]; int i;
        giveMeTheTrail(gs, PLAYER_DRACULA, dracTrail);
        LocationID myLoc = whereIs(gs, me);
        
        if (myLoc == dracTrail[0] && myHealth <= 2) {
            // fighting drac and low health
            LocationID j, numLocs = 0;
            LocationID *whereTo = whereCanIgo(gs, &numLocs, 1, NO_RAIL_MOVES, 1);
            for (i = 0; i < 30; i++) {
                j = whereTo[rand()%numLocs];
                if (j == myLoc) continue;
                else break;
            }
            if (i == 30) {
                j = whereTo[rand()%numLocs];
                if (numLocs > 2 && whereTo[1] != -1) {
                    j = whereTo[1]; // hopefully doesnt happen
                }
                printf("pls dun die\n");
            }
            
            free(whereTo);
            registerBestPlay(idToAbbrev(j), "0008");
            return;
        }
        
        else if (myHealth < 6) {
            registerBestPlay(idToAbbrev(whereIs(gs, me)), "0006");
            return;
        }
    }

    
    // RESEARCH override
    //   researches on rounds after vampire (unfortunately) matures and
    //   before the next one spawns. somewhat. see code
    {   
        int i, dracUnknown = TRUE;
        LocationID dracTrail[6];
        giveMeTheTrail(gs, PLAYER_DRACULA, dracTrail);
        for (i = 0; i < TRAIL_SIZE; i++) {
            if (validPlace(dracTrail[i])) {
                dracUnknown = FALSE;
                break;
            }
        }
        
        if (dracUnknown) {
            int roundOK = FALSE;
            Round roundMod = giveMeTheRound(gs);
            roundMod = roundMod % 26;
            
            // RESEARCH ROUND MODIFIERS
            if (roundMod > TRAIL_SIZE && roundMod < 13)
                roundOK = TRUE;
            // if (roundMod > (TRAIL_SIZE + 13) && roundMod < 26)
            //     roundOK = TRUE;
            
            if (roundOK && dracUnknown) {
                // go research
                registerBestPlay(idToAbbrev(whereIs(gs, whoAmI(gs))), "0007");
                return;
            }
        }
    }

    
    // Improved drac chaser using shortest path
    // by analysing draculas trail
    //   calculates HIDE and DBack moves
    //   accounts for TP/ CASTLE_DRACULA
    //   moves only to drac range/radius relative to 'freshness' of dracTrail
    //   does nothing if trail contains no known info 
    {
        LocationID dracTrail[6]; int i, radius = 0;
        giveMeTheTrail(gs, PLAYER_DRACULA, dracTrail);
        int targetLoc = UNKNOWN_LOCATION;

        for (i = 0; i < TRAIL_SIZE; i++, radius++) {
            int d = dracTrail[i];
            if (d == UNKNOWN_LOCATION) {
                break; // trail empty
            }
            else if (d == CITY_UNKNOWN || d == SEA_UNKNOWN) {
                continue;
            }
            else if (d == HIDE) {
                // attempts to calculate hide location
                if(i > 0 && validPlace(dracTrail[i-1])) {
                    targetLoc = dracTrail[i-1];
                    if (radius > 0) radius--;
                    break;
                }
                else continue;
            }
            else if (d >= DOUBLE_BACK_1 && d <= DOUBLE_BACK_5) {
                // attempts to calculate dback location 
                if (i > (d - 103) && validPlace(dracTrail[i-d-103])) {
                    targetLoc = dracTrail[i-d-103];
                    radius -= (d - 103 + 1);
                    break;
                }
                continue;
            }
            else if (d == CASTLE_DRACULA || d == TELEPORT) {
                targetLoc = CASTLE_DRACULA;
                break;
            }
            else {
                targetLoc = d; // found revealed location
                break;
            }
        }
        
        if (targetLoc != UNKNOWN_LOCATION && validPlace(targetLoc)) {
            // find shortest path, move to
            int i, currLoc = whereIs(gs, whoAmI(gs)), numLocs = 0;
            LocationID path[200]; for (i = 0; i < 200; i++) path[i] = A_INIT;
            LocationID bans[100]; for (i = 0; i < 100; i++) bans[i] = A_INIT;
            shortestPath(currLoc, targetLoc, path, bans, &numLocs, 1, NO_RAIL_MOVES, 1);

            if (numLocs > 2) {
                // populate bans[]
                int i;
                for (i = 0; i < 200; i++) path[i] = bans[i] = A_INIT; 
                
                for (i = 0; i < (NUM_PLAYERS-1); i++) {
                    LocationID playerTrail[TRAIL_SIZE] = {'\0'};
                    giveMeTheTrail(gs, i, playerTrail);

                    if (validPlace(playerTrail[0]) && 
                        playerTrail[0] != targetLoc && playerTrail[0] != currLoc)
                        append(bans, playerTrail[0]);
                    

                }
                shortestPath(currLoc, targetLoc, path, bans, 
                    &numLocs, 1, NO_RAIL_MOVES, 1);

            }
            
            if (currLoc == targetLoc && radius == 0) {
                // "should be fighting drac"
                registerBestPlay(idToAbbrev(targetLoc), "0005");
                return;
            }
            else if (currLoc != targetLoc && radius < numLocs) {
                // "enroute to drac"
                registerBestPlay(idToAbbrev(path[1]), "0001");
                return;
            }            
            else if (radius < 3) {
                // "close to fighting drac"
                registerBestPlay(idToAbbrev(targetLoc), "0002");
                return;
            }
        } 

        // else do nothing
        // proceed to PRNG move
    }
    
    
    // PRNG based random location picker
    //   move to locations that hadnt been to the last 3 turns
    //   will not move to where other players were at (PREV_CAP) rounds ago
    //   rerolls capped at (REROLL_CAP)
    #define PREV_CAP 2
    #define REROLL_CAP 30
    {
        LocationID numLocs = 0;
        LocationID *whereTo = whereCanIgo(gs, &numLocs, 1, NO_RAIL_MOVES, 1);
        
        if (numLocs > 0 && giveMeTheRound(gs) > 0) {
            LocationID j, myTrail[TRAIL_SIZE]; int i;
            giveMeTheTrail(gs, whoAmI(gs), myTrail);
            myTrail[3] = A_INIT; myTrail[4] = A_INIT; myTrail[5] = A_INIT;  // hard limiter
            
            LocationID otherHunters[NUM_PLAYERS-1];

            int m = 0;  // populate array for no player overlaps
            for (i = 0; i < (NUM_PLAYERS-1); i++) {
                LocationID trail[TRAIL_SIZE];
                int v; for (v = 0; v < TRAIL_SIZE; v++) trail[v] = A_INIT;
                giveMeTheTrail(gs, i, trail);
                int k;
                for (k = 0; k < PREV_CAP; k++) {
                    if (trail[k] != A_INIT) otherHunters[m++] = trail[k];
                }
            }

            for (i = 0; i < REROLL_CAP; i++) {
                j = whereTo[rand()%numLocs];
                if (existInArray(myTrail, j) || existInArray(otherHunters, j))
                    continue;
                else break;
            }
            if (i == REROLL_CAP) {
                j = whereTo[rand()%numLocs];
                if (numLocs > 2 && (whereTo[1] != -1 || whereTo[1] != '\0')) {
                    j = whereTo[1]; // printf("wut is this.. ");
                }
                printf("unlucky rolls/ Im stuck\n");
            }
            
            registerBestPlay(idToAbbrev(j), "0003");
            
        }
        else {
            // shouldn't ever need this. keep this here just in case
            registerBestPlay(idToAbbrev(rand()%NUM_MAP_LOCATIONS), "0100");
        }
        
        free(whereTo);
    }
    
}



// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 
// @@@@@@@@@@@@@@@@@@@@@     PRIVATE  FUNCTIONS     @@@@@@@@@@@@@@@@@@@@@ 
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 

static void shortestPath(LocationID start, LocationID end, 
LocationID path[200], LocationID bans[100], LocationID *numLocs, 
int road, int rail, int sea)
{
    assert(path != NULL && numLocs != NULL);
    if (!road && !rail && !sea) {   // lel wut
        *numLocs = 0; path[0] = A_INIT;
        return;
    }
    
    rail = NO_RAIL_MOVES;
    
    if (start == end) {
        path[0] = start; path[1] = end;
        *numLocs = 1; return;
    }
    Map g = newMap();

    if (bans[0] != A_INIT) {
    
        int i; printf("bans: ");
        for (i = 0; bans[i] != A_INIT; i++) printf("%d ", bans[i]); 
        printf("\n");
    
        printf("running exceptions\n");
        rmFromArray(bans, start);
        rmFromArray(bans, end);
        
        // SPECIAL CASES
        //  loc:  0,  2,  3,  5,  6,  9, 10, 16, 17, 21, 22,
        //       24, 27, 30, 38, 41, 47, 51, 52, 54, 61, 66
        rmFromArray(bans, ADRIATIC_SEA);
        if (start == AMSTERDAM || end == AMSTERDAM) rmFromArray(bans, BRUSSELS); // AMSTERDAM
        if (start == ATHENS || end == ATHENS) rmFromArray(bans, VALONA); // ATHENS
        if (start == BARCELONA || end == BARCELONA) rmFromArray(bans, SARAGOSSA); // BARCELONA
        if (start == BARI || end == BARI) rmFromArray(bans, NAPLES); // BARI
        if (start == BERLIN || end == BERLIN) rmFromArray(bans, LEIPZIG); // BERLIN
        rmFromArray(bans, BLACK_SEA);
        if (start == CAGLIARI || end == CAGLIARI) rmFromArray(bans, MEDITERRANEAN_SEA); // CAGLIARI
        if (start == CASTLE_DRACULA || end == CASTLE_DRACULA) rmFromArray(bans, GALATZ); // CASTLE_DRACULA
        if (start == DUBLIN || end == DUBLIN) rmFromArray(bans, IRISH_SEA); // DUBLIN
        if (start == EDINBURGH || end == EDINBURGH) rmFromArray(bans, MANCHESTER); // EDINBURGH
        
        if (start == FLORENCE || end == FLORENCE) rmFromArray(bans, VENICE); // FLORENCE
        if (start == GALWAY || end == GALWAY) rmFromArray(bans, ATLANTIC_OCEAN); // GALWAY
        if (start == GRANADA || end == GRANADA) rmFromArray(bans, MADRID); // GRANADA
        if (start == LIVERPOOL || end == LIVERPOOL) rmFromArray(bans, IRISH_SEA); // LIVERPOOL
        if (start == MANCHESTER || end == MANCHESTER) rmFromArray(bans, LONDON); // MANCHESTER
        if (start == NAPLES || end == NAPLES) rmFromArray(bans, ROME); // NAPLES
        if (start == PLYMOUTH || end == PLYMOUTH) rmFromArray(bans, ENGLISH_CHANNEL); // PLYMOUTH
        if (start == PRAGUE || end == PRAGUE) rmFromArray(bans, VIENNA); // PRAGUE
        if (start == SALONICA || end == SALONICA) rmFromArray(bans, SOFIA); // SALONICA
        if (start == SWANSEA || end == SWANSEA) rmFromArray(bans, LONDON); // SWANSEA
        if (start == VARNA || end == VARNA) rmFromArray(bans, SOFIA); // VARNA
    }
    
    if (0) {
        BANS_OVRIDE: 
        printf("warning. overriding bans\n");
        bans[0] = A_INIT; bans[1] = A_INIT;
        // this scraps the whole bans system
        // since accessing array[-1] will segfault
        // suggestions to improve this is hugely welcome
    }
    
    int i, order = 0, visited[g->nV], prior[g->nV];
    Queue bfsQueue = newQueue();
    Edge sourceEdge;
    sourceEdge.start = start;
    sourceEdge.end = start;
    sourceEdge.type = NONE;
    
    QueueJoin (bfsQueue, sourceEdge);
    prior[sourceEdge.start] = order++;
    for (i = 0; i < g->nV; i++) {
        visited[i] = -1; 
        prior[i] = -1;
    }
    
    while (!QueueIsEmpty(bfsQueue)) {
        Edge next = QueueLeave(bfsQueue);
        visited[next.end] = next.start;
        int v;
        for (v = 0; v < g->nV; v++) {
            if (!existInList(g, next.end, v)) 
                continue;
            
            if (existInArray(bans, v)) continue;
            int tr = NONE;
            if (transportType(g, next.end, v, ROAD)) {
                if (!road) continue;
                tr = ROAD;
            }
            else if (transportType(g, next.end, v, BOAT)) {
                if (!sea) continue;
                tr = BOAT;
            }
            else if (transportType(g, next.end, v, RAIL)) {
                if (!rail) continue;
                tr = RAIL;
            } 
            else if (transportType(g, next.end, v, ANY)) {
                tr = ROAD;
            }

            if (prior[v] == -1) {
                Edge tempE;
                tempE.start = next.end;
                tempE.end = v;
                tempE.type = tr;
                QueueJoin(bfsQueue, tempE);
                prior[v] = order++;
            }
        }
    }
    
    int curr = end, counter = 0;
    while (curr != start) {
        path[counter++] = curr;
        if (curr < 0) goto BANS_OVRIDE; // pls dont kill me 
        curr = visited[curr];
    }
    path[counter++] = start;
    for (i = 0; i < counter/2; i++) {
        int temp = path[i];
        path[i] = path[counter - i - 1];
        path[counter - i - 1] = temp;
    }
    *numLocs = counter;
    dropQueue(bfsQueue);
    disposeMap(g);
}


// ARRAY OP FUNCTIONS
static void rmFromArray(int *array, int v)
{
    if (array[0] == A_INIT) return;
    
    int i = 0;
    while (array[i] != v && array[i] != A_INIT) i++;
    
    if (array[i] == A_INIT) return;
    else if (array[i] == v) {
        while (array[i+1] != A_INIT) {
            array[i] = array[i+1];
            i++;
        }
        array[i] = A_INIT;
    }
    else return;

    rmFromArray(array, v);
}

static int existInArray(int *array, int v)
{
    if (array[0] == A_INIT) return FALSE;
    int i;
    for (i = 0; array[i] != A_INIT; i++) {
        if (array[i] == v) return TRUE;
    }
    return FALSE;
}

static void append(int *array, int v)
{
    int i = 0;
    while (array[i] != A_INIT) {
        i++;
    }
    array[i] = v;
    array[i+1] = A_INIT;
}
/*
static int arrayLength(int *array)
{
    if (array[0] != A_INIT) return 0;
    int i = 0;
    while (array[i] != A_INIT) i++;
    return i;
}

static int pop(int *array)
{
    if (array[0] == A_INIT) return A_INIT;
    int i = 0;
    while (array[i] != A_INIT) {
        i++;
    }
    int j = array[i-1];
    array[i-1] = A_INIT;
    return j;
}

static int lastNode(int *array)
{
    if (array[0] == A_INIT) return A_INIT;
    int i = 0;
    while (array[i] != A_INIT) i++;
    return array[i-1];
}
*/

// MAP SEARCH FUNCTIONS
static int existInList(Map g, int source, int target)
{
    VList curr = g->connections[source];
    while (curr != NULL) {
        if (curr->v == target) return TRUE;
        else curr = curr->next;
    }
    return FALSE;
}

// boolean. determines if source->target has a matching link of ttype
static int transportType(Map g, int source, int target, int ttype)
{
    VList curr = g->connections[source];
    while (curr != NULL) {
        if (curr->v == target && curr->type == ttype)
            return TRUE;
        curr = curr->next;
    }
    return FALSE;
}


// QUEUE OP FUNCTIONS
static void QueueJoin(Queue Q, Edge it)
{
	assert(Q != NULL);
	QueueNode *new = malloc(sizeof(QueueNode));
	assert(new != NULL);
	new->value = it;
	new->next = NULL;
	if (Q->head == NULL)
		Q->head = new;
	if (Q->tail != NULL)
		Q->tail->next = new;
	Q->tail = new;
}

static Edge QueueLeave(Queue Q)
{
	assert(Q != NULL);
	assert(Q->head != NULL);
	Edge it = Q->head->value;
	QueueNode *old = Q->head;
	Q->head = old->next;
	if (Q->head == NULL)
		Q->tail = NULL;
	free(old);
	return it;
}

static Queue newQueue()
{
	Queue q;
	q = malloc(sizeof(QueueRep));
	assert(q != NULL);
	q->head = NULL;
	q->tail = NULL;
	return q;
}

static void dropQueue(Queue Q)
{
	QueueNode *curr, *next;
	assert(Q != NULL);
	// free list nodes
	curr = Q->head;
	while (curr != NULL) {
		next = curr->next;
		free(curr);
		curr = next;
	}
	// free queue rep
	free(Q);
}

static int QueueIsEmpty(Queue Q)
{
	return (Q->head == NULL);
}


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 
// @@@  PRINT DEBUGGING INFO
static void printDebug(HunterView gs)
{
    printf("## PRINTING DEBUG..\n");
    
    LocationID dracTrail[6]; int i, me = whoAmI(gs);
    giveMeTheTrail(gs, PLAYER_DRACULA, dracTrail);
    printf("#  drac trail checks:\n");
    for (i = 0; i < TRAIL_SIZE; i++) {
        int d = dracTrail[i]; printf("#   %d", i);
        if (d == CITY_UNKNOWN)
            printf(" dunno which city drac is in\n");
        else if (d == SEA_UNKNOWN)
            printf(" dunno which sea drac is swimming in\n");
        else if (d == UNKNOWN_LOCATION)
            printf(" nothing known yet\n");
        else if (d >= DOUBLE_BACK_1 && d <= DOUBLE_BACK_5)
            printf(" drac went dback %d steps!\n", (d - 102));
        else if (d == HIDE)
            printf(" sneaky drac been hiding..\n");
        else if (d == CASTLE_DRACULA || d == TELEPORT)
            printf(" drac was at his castle %d rounds ago\n", i);
        else {
            printf(" drac was last encountered at %d", d);
            if (d >= MIN_MAP_LOCATION && d <= MAX_MAP_LOCATION) 
                printf(" %s", idToAbbrev(d));
            printf(", %d rounds ago\n", i);
        }
    }
    
    printf("#  current round num %d\n", giveMeTheRound(gs));
    printf("#  current game score %d\n", giveMeTheScore(gs));
    printf("#  dracula health at %d\n", howHealthyIs(gs, PLAYER_DRACULA));
    printf("#  Im player %d\n", me);
    
    for (i = 0; i < NUM_PLAYERS-1; i++) {
        int loc = whereIs(gs, i);
        printf("#  player %d, health %d, at %2d", i, howHealthyIs(gs, me), loc);
        if (loc >= MIN_MAP_LOCATION && loc <= MAX_MAP_LOCATION) 
            printf(" %s, ", idToAbbrev(loc));
        else 
            printf(",    ");
        
        if (loc == dracTrail[0] && loc != UNKNOWN_LOCATION)
            printf("YES fighting drac\n");
        else
            printf("NOT fighting drac\n");
        
    }
    
    printf("#\n");
}

