// Map.h ... interface to Map data type

#ifndef MAP_H
#define MAP_H

#include "Places.h"

#define NO_EDGE -1
#define NUM_TRANSPORT (ANY+1)

typedef struct edge{
    LocationID  start;
    LocationID  end;
    TransportID type;
} Edge;


// Graph representation is NO LONGER hidden 
// otan please don't kill me
typedef struct MapRep *Map; 
typedef struct vNode *VList;

struct vNode {
   LocationID  v;    // ALICANTE, etc
   TransportID type; // ROAD, RAIL, BOAT
   VList       next; // link to next node
};

struct MapRep {
   int   nV;         // #vertices
   int   nE;         // #edges
   VList connections[NUM_MAP_LOCATIONS]; // array of lists
   
   // immediate edges: num edges via each mode of transport
    int adjmat[NUM_TRANSPORT][NUM_MAP_LOCATIONS][NUM_MAP_LOCATIONS];
};
// end map_c extracts


// -- Operations on graphs --
// Create a new empty graph (for a map)
Map  newMap();  

// Remove an existing map
void disposeMap(Map g); 

// Display content of map (prints to stdio)
void showMap(Map g); 

// Return count of nodes
int  numV(Map g);

// Return count of edges of a particular type
int  numE(Map g, TransportID t);

// returns the distance of a direct edge of transport t FROM a TO b
// or NO_EDGE if no such edge exists
int  getDist(Map g, TransportID t, int a, int b);

#endif
